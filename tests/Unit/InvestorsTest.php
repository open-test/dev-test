<?php

namespace Tests\Unit;

use App\Classes\Investor;
use App\Classes\Loan;
use App\Classes\Tranche;
use PHPUnit\Framework\TestCase;

class InvestorsTest extends TestCase
{

  public function setUp(): void 
  {
  	parent::setUp();
    $this->investor = new Investor(1000);
    $this->tranche = new Tranche("A", 3);
  }

  public function test_cash_exists()
  {
    $cash = property_exists($this->investor, "cash");
    $this->assertSame(true, $cash);
  }

  public function test_get_loan() 
  {
    $loan = $this->investor->getLoan();
    $this->assertSame(0, $loan);
  }

  public function test_make_investment()
  {
    $this->assertSame("ok", $this->investor->makeInvestment(500, $this->tranche, "2020-10-03")['message']);
    $this->assertSame(500, $this->investor->cash);
    $this->assertInstanceOf(Loan::class, $this->investor->loan);
  }

  public function test_calculate_interest()
  {
    $this->investor->makeInvestment(1000, $this->tranche, "2020-10-03");
    $this->assertSame(28.06, $this->investor->calculateInterest());
  }
}
