<?php

namespace Tests\Unit;

use App\Classes\Investor;
use App\Classes\Loan;
use App\Classes\Tranche;
use PHPUnit\Framework\TestCase;

class LoansTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function setUp(): void
    {
       parent::setUp();
	   $this->loan = new Loan("2020-10-03", 3, 1000);
	}

	public function test_start_date_exists() 
	{
		$start_date = property_exists($this->loan, "start_date");
		$this->assertSame(true, $start_date);
		$this->assertEquals(strtotime("2020-10-03"), $this->loan->start_date);
	}

	public function test_interest_rate_exists() 
	{
		$interest_rate = property_exists($this->loan, "interest_rate");
		$this->assertEquals(true, $interest_rate);
	}

	public function test_amount_exists() 
	{
		$amount = property_exists($this->loan, "amount");
		$this->assertEquals(true, $amount);
	}

	public function test_calculate_days_invested()
	{
    	$this->assertEquals(29, $this->loan->calculateDaysInvested());
	}

	public function test_calculate_percentage_of_month_invested() 
	{
	    $this->assertEquals(0.935483870968, $this->loan->calculatePercentageOfMonthInvested());
	}

	public function test_calculate_full_month_interest() 
	{
	    $this->assertEquals(30, $this->loan->calculateFullMonthInterest());
	}

	public function test_calculate_interest() 
	{
	    $this->assertEquals(28.06, $this->loan->calculateInterest());
	}
}
