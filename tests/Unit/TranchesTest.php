<?php

namespace Tests\Unit;

use App\Classes\Investor;
use App\Classes\Loan;
use App\Classes\Tranche;
use PHPUnit\Framework\TestCase;

class TranchesTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
      public function setUp(): void 
      {
      	parent::setUp();
	    $this->tranche = new Tranche("B", 6);
	  }

	  public function test_funds_available_exists()
	  {
	    $funds_available = property_exists($this->tranche, "funds_available");
	    $this->assertSame(true, $funds_available);
	  }

	  public function test_amount_invested_exists()
	  {
	    $amount_invested = property_exists($this->tranche, "amount_invested");
	    $this->assertSame(true, $amount_invested);
	  }

	  public function test_balance_available()
	  {
	    $this->assertSame(1000, $this->tranche->getBalanceAvailable());
	  }

	  public function test_interest_rate_exists()
	  {
	    $interest_rate = property_exists($this->tranche, "interest_rate");
	    $this->assertSame(true, $interest_rate);  
	  }

	  public function test_add_funds()
	  {
	    $this->tranche->addFunds(1000);
	    $this->assertSame(1000, $this->tranche->amount_invested);
	    $this->assertSame("error", $this->tranche->addFunds($this->tranche->amount_invested));
	    $this->tranche->addFunds(1);
	  }
}
