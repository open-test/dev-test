<?php

namespace App\Http\Controllers;


use App\Classes\Investor;
use App\Classes\Loan;
use App\Classes\Tranche;
use Illuminate\Http\Request;

class Home extends Controller
{


    public function index()
    {

        // Initialise Investors with cash (wallet balance)
    	$investor1 = new Investor(1000);         
	    $investor2 = new Investor(1000);
	    $investor3 = new Investor(1000);
	    $investor4 = new Investor(1000);
        
        // Investors array
	    $investors = [
	    	"1" => $investor1,         
		    "2" => $investor2,
		    "3" => $investor3,
		    "4" => $investor4,
	    ];

        // Initialise Tranche with type and interest rate.
        $trancheA = new Tranche("A", 3);
        $trancheB = new Tranche("B", 6);
        
        // Tranches array
        $tranches = [
           "a" => $trancheA,
           "b" => $trancheB,
        ];

        // Investments array with amount, tranche, start_date and end_date.
        $investments = [
			"1" => $investor1->makeInvestment(1000, $trancheA, "2020-10-03"),               ## return "ok"
			"2" => $investor2->makeInvestment(100, $trancheA, "2020-10-04"),                ## return Exception... 
			"3" => $investor3->makeInvestment(500, $trancheB, "2020-10-10"),                ## return "ok"
			"4" => $investor4->makeInvestment(1100, $trancheB, "2020-10-25"),               ## return Exception... 
        ];
        
        // Results array for calculated interest 
        $results = [
	       "1" => $investor1->calculateInterest(), ## return 28.06
           "3" => $investor3->calculateInterest(), ## return 21.29
        ];

	    return view("welcome", compact("investors","tranches","investments","results"));
    }


}
