<?php

namespace App\Classes;

use Illuminate\Validation\ValidationException;
use Exception;

class Tranche
{

  public $type;
  public $funds_available = 1000;
  public $amount_invested = 0;
  public $interest_rate;

  function __construct($type, $interest_rate) 
  {
    $this->type = $type;
    $this->interest_rate = $interest_rate;
  }

  public function getInterestRate() 
  {
    return $this->interest_rate;
  }
  
  // Return available funds balance
  public function getBalanceAvailable() 
  {
    return $this->funds_available - $this->amount_invested;
  }

  // Add funds
  public function addFunds($amount) 
  {

    $newAmount = $this->amount_invested + $amount;

    if ($newAmount <= $this->funds_available) 
    {
      $this->setAmountInvested($newAmount);
      return 'ok';
    } else {
      return 'error';
    }
  }
  
  // Set invested amount
  private function setAmountInvested($amount) 
  {
    $this->amount_invested = $amount;
  }


}
