<?php

namespace App\Classes;

use App\Classes\Loan;

class Investor
{

  public $cash; ## wallet
  public $loan = 0;

  function __construct($cash) 
  {
    $this->cash = $cash;
  }

  // Return loan
  public function getLoan() 
  {
    return $this->loan;
  }
  
  // Add funds, create loan, decrease cash balance in wallet
  public function makeInvestment($amount, $tranche, $start_date) 
  {
    $funded = $tranche->addFunds($amount);

    if ($funded == "ok") 
    {
      $loan = $this->createLoan($start_date, $tranche->getInterestRate(), $amount);
      $this->decreaseBalance($amount);
      return ["amount" => $amount, "tranche" => $tranche, "date" => $start_date, "message" => "ok"];

    }

    return ["amount" => $amount, "tranche" => $tranche, "date" => $start_date, "message" => "exception"];

  }
  
  // calculate interest rate
  public function calculateInterest() 
  {
    return $this->loan->calculateInterest();
  }

  // Decrease cash balance
  private function decreaseBalance($amount) 
  {
    $this->cash = $this->cash - $amount;
  }
  
  // Create loan 
  private function createLoan($start_date, $interest_rate, $amount) 
  {
    $this->loan = new Loan($start_date, $interest_rate, $amount);
  }

}
