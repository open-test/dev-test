<?php

namespace App\Classes;

use Carbon\Carbon;

class Loan
{

  public $start_date;
  public $end_date;
  public $interest_rate;
  public $amount;

  function __construct($start_date, $interest_rate, $amount) 
  {

    $this->start_date = strtotime($start_date);
    $this->end_date =  strtotime("2020-10-31");
    $this->interest_rate = $interest_rate;
    $this->amount = $amount;
  }
  
  // Calculate days invested ie. period
  public function calculateDaysInvested() 
  {

    $period = $this->end_date - $this->start_date;
    $invested_days = floor($period/60/60/24) + 1;

    return $invested_days;

  }
  
  // Calculate percentage of days invested within a month
  public function calculatePercentageOfMonthInvested() 
  {

    $invested_days = $this->calculateDaysInvested();
    $month_days = 31; // could be improved with Carbon
    $perc_of_month_invested = $invested_days / $month_days;

    return $perc_of_month_invested;

  }
  
  // Calculate interest for month
  public function calculateFullMonthInterest() 
  {
    
    $month_interest = $this->amount * ($this->interest_rate / 100);

    return $month_interest;

  }

  public function calculateInterest() 
  {

    $month_interest = $this->calculateFullMonthInterest();
  
    $perc_of_month_invested = $this->calculatePercentageOfMonthInvested();

    $nominal_interest = $month_interest * $perc_of_month_invested;

    return round($nominal_interest, 2);
  }


}
